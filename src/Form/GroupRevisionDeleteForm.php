<?php

namespace Drupal\group_diff_ui\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_diff_ui\Utility\RevisionOperationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a group content revision.
 *
 * @internal
 */
class GroupRevisionDeleteForm extends ConfirmFormBase {

  use RevisionOperationTrait;

  /**
   * The group content revision.
   *
   * @var \Drupal\Core\Entity\EditorialContentEntityBase
   */
  protected $revision;

  /**
   * The group content storage.
   *
   * @var EntityStorageInterface
   */
  protected $groupStorage;
  
  /**
   * The group type storage.
   *
   * @var EntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  
  /**
   * The database connection object.
   * 
   * @var \Drupal\Core\Database\Connection
   */
  protected  $connection;

  /**
   * Constructs a new GroupRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $group_storage
   *   The group content storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $group_type_storage
   *   The group type storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityStorageInterface $group_storage, EntityStorageInterface $group_type_storage, Connection $connection, DateFormatterInterface $date_formatter) {
    $this->groupStorage = $group_storage;
    $this->groupTypeStorage = $group_type_storage;
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('group'),
      $entity_type_manager->getStorage('group_type'),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.group.version_history', ['group' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $group_revision = NULL) {
    $this->revision = $this->groupStorage->loadRevision($group_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->revisionDelete($this->groupStorage, $this->groupTypeStorage, $this->revision, $this->logger('group'), $this->messenger());
    $form_state->setRedirect(
        'entity.group.canonical',
        ['group' => $this->revision->id()]
        );
    if ($this->connection->query('SELECT COUNT(DISTINCT revision_id) FROM {group_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.group.version_history',
        ['group' => $this->revision->id()]
      );
    }
  }

}
