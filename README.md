Provide Diff UI plugin for Group entity.

Similar to what https://www.drupal.org/project/entity_diff_ui does.

# MODULE STATUS

This module works, but the code is not in a polished state.

It was put in place in an extreme hurry and needs review
and possibly refactoring.

# CONTRIBUTOR INFORMATION NEEDED

This was not originally developed by me (maxwellkeeble), though I have made some fixes.
I originally found it on a public github repository which I can no longer find,
and sadly I did not keep a record of it.

It may be related to this issue https://www.drupal.org/project/diff/issues/3087300

Please contact me if you are the original developer so I can provide proper credit
and maintainership if needed.
